﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreCounter : MonoBehaviour {

	private static ScoreCounter self;
	public static ScoreCounter instance{
		get{
			if (self == null) {
				self = (ScoreCounter)GameObject.FindObjectOfType (typeof(ScoreCounter));
			}
			return self;
		}
	}

	private int score =0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Text> ().text = score.ToString ();;
	}

	public void incrementScore(){
		score++;
	}

}
