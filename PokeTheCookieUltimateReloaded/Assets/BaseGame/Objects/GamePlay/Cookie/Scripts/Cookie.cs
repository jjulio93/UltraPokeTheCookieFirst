﻿using UnityEngine;
using System.Collections;

public class Cookie : MonoBehaviour {

	private bool isPokeable;

	// Use this for initialization
	void Start () {
		isPokeable = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnPoke(){
		if (isPokeable) {
			StartCoroutine (processPoke ());
		}
	}

	public IEnumerator processPoke(){
		transform.localScale *= .8f;
		ScoreCounter.instance.incrementScore ();
		isPokeable = false;

		yield return new WaitForSeconds(0.05f);

		transform.localScale = Vector3.one;
		isPokeable = true;
	}

}
