﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	private static GameController self;
	public static GameController instance{
		get{
			if (self == null) {
				self = (GameController)GameObject.FindObjectOfType (typeof(GameController));
			}
			return self;
		}
	}

	public GameObject PausePanel;

	[HideInInspector]
	public bool isGamePaused = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void pauseGame(){
		PausePanel.SetActive (true);
		isGamePaused = true;
	}

	public void resumeGame(){
		PausePanel.SetActive (false);
		isGamePaused = false;
	}

}
