﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

	private static Timer self;
	public static Timer instance{
		get{
			if (self == null) {
				self = (Timer)GameObject.FindObjectOfType (typeof(Timer));
			}
			return self;
		}
	}

	private float time =0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameController.instance.isGamePaused) {
			time += Time.deltaTime;
			GetComponent<Text> ().text = Mathf.Floor(time).ToString();
		}
	}
}
